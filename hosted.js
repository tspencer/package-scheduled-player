"use strict";

/*
 * Copyright (C) 2016,2017 Florian Wesch <fw@dividuum.de>
 *
 * If you want to develop your own custom config, have a
 * look at https://github.com/info-beamer/package-sdk
 * instead. It contains a hosted.js implementation that
 * is intended for standalone use.
 */

(function() {
  var asset_root = decodeURIComponent(window.location.hash.substring(1));
  console.log("[HOSTED.JS] asset root is ", asset_root);

  function Deferred() {
    this.resolve = null;
    this.reject = null;
    this.promise = new Promise(function(resolve, reject) {
        this.resolve = resolve;
        this.reject = reject;
    }.bind(this));
    Object.freeze(this);
  }

  var head = document.getElementsByTagName("head")[0];
  var body = document.getElementsByTagName("body")[0];

  var meta = document.createElement('meta');
  meta.setAttribute("name", 'referrer');
  meta.setAttribute("content", 'no-referrer');
  head.appendChild(meta);

  // var tag = document.createElement('div');
  // tag.setAttribute("data-iframe-height", "yes");
  // body.appendChild(tag);

  function setupResources(js, css) {
    for (var idx = 0; idx < js.length; idx++) {
      var script = document.createElement('script');
      script.setAttribute("type","text/javascript");
      script.setAttribute("src", asset_root + 'js/' + js[idx]);
      head.appendChild(script);
    }

    for (var idx = css.length-1; idx >= 0; idx--) {
      var link = document.createElement('link')
      link.setAttribute('rel', 'stylesheet')
      link.setAttribute('type', 'text/css')
      link.setAttribute('href', asset_root + 'css/' + css[idx])
      head.insertBefore(link, head.firstChild);
    }
  }

  // load promise module
  setupResources(['promise-7.0.4.min.js'], []);

  // deferred for when all data (assets, config)
  // is readily available in 'ib'
  var ready = new Deferred();

  // Legacy deferred that triggers when the
  // config is available / getConfig gets
  // called.
  var config_available = new Deferred();

  // Token that is used to make sure that this
  // iframe is permitted to talk to the parent.
  var token = '';

  // Base url for documentation links for the
  // current package.
  var doc_link_base = '';

  // Public "object"
  var ib = {
    config: undefined,
    assets: {},
    node_assets: {},
    devices: [],
    node_path: '',
    apis: {},
    device: null,
  }

  ib.setDefaultStyle = function() {
    setupResources([], ['reset.css', 'bootstrap.css'])
  }

  ib.ready = ready.promise;

  function send_to_parent(type, data) {
    var send_to_parent = 'parentIFrame' in window;
    if (send_to_parent) {
      parentIFrame.sendMessage(JSON.stringify({
        'type': type,
        'token': token,
        'data': data,
      }));
    }
  }

  ib.setConfig = function(config) {
    ib.config = config;
    send_to_parent('config', ib.config);
  }

  ib.sendCommand = function(suffix, value) {
    var path = ib.node_path + (ib.node_path == "" ? "" : "/") + suffix;
    if (value == undefined)
      value = "";
    if (typeof(value) == "object")
      value = JSON.stringify(value);
    send_to_parent('command', {
      path: path,
      value: value,
    })
  }

  ib.getConfig = function(cb) {
    console.warn("[HOSTED.JS] using .getConfig is deprecated. Use .ready.then(...) instead");
    config_available.promise.then(function() {
      cb(ib.config);
    });
  }

  ib.getDocLink = function(name) {
    return doc_link_base + name;
  }

  function wrap_api(api_name, api_url) {
    return {
      url: api_url,
      get: function(params) {
        params = params || {};
        params = Object.keys(params).map(function(k) {
          return encodeURIComponent(k) + '=' + encodeURIComponent(params[k]);
        }).join('&');
        return new Promise(function(resolve, reject) {
          var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
          xhr.open('GET', api_url + (params == '' ? '' : '?' + params));
          xhr.onreadystatechange = function() {
            if (xhr.readyState > 3) {
              if (xhr.status == 200) {
                resolve(JSON.parse(xhr.responseText));
              } else {
                var err = xhr.responseText;
                try {
                  err = JSON.parse(xhr.responseText)['error'];
                } catch(e) {}
                reject(Error("Status " + xhr.status + ": " + err));
              }
            };
          }
          xhr.onerror = function() {
            reject(Error("Network Error"));
          };
          xhr.setRequestHeader('X-Requested-With', 'hosted.js');
          xhr.send();
        })
      }
    }
  }

  function wrap_apis(apis) {
    var out = {};
    for (var api_name in apis) {
      out[api_name] = wrap_api(api_name, apis[api_name]);
    }
    return out;
  }

  // Export ib to the iframe
  window.infobeamer = window.ib = ib;

  window.iFrameResizer = {
    messageCallback: function(msg) {
      var msg = JSON.parse(msg);
      if (msg.type == 'config') {
        var config_already_set = ib.config != undefined;
        ib.config = config_already_set ? ib.config : msg.data;
        if (config_already_set)
          ib.setConfig(ib.config);
        config_available.resolve();
      } else if (msg.type == 'assets') {
        ib.assets = msg.data;
      } else if (msg.type == 'node_assets') {
        ib.node_assets = msg.data;
      } else if (msg.type == 'devices') {
        ib.devices = msg.data;
      } else if (msg.type == 'doc_link_base') {
        doc_link_base = msg.data;
      } else if (msg.type == 'device') {
        ib.device = msg.data;
      } else if (msg.type == 'node_path') {
        ib.node_path = msg.data;
      } else if (msg.type == 'apis') {
        ib.apis = wrap_apis(msg.data);
      } else if (msg.type == 'token') {
        token = msg.data;
      } else if (msg.type == 'ready') {
        console.log("[HOSTED.JS] config iframe ready");
        ready.resolve();
      }
    },
    readyCallback: function() {
      console.log("[HOSTED.JS] iframe ready");
    }
  }

  setupResources(['iframeResizer.contentWindow.js'], []);

  console.log('[HOSTED.JS] initialized');
})();
